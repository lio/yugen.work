let other = {
	kind: 'Other',
	points: [
		"I need a Reference Sheet! The only exception is if I am making a reference sheet FOR you, or its artistic liberty but even then i prefer to have a crude drawing with color done on MS paint over nothing. It doesn't have to be fancy, I just am not willing to read a 2000 word document about the intricacies of your character.",
		'Once you commission me, or buy an adopt from me, the art that i have drawn for you is yours! The only thing that i ask is that you do not claim it as your own, trace, steal, resell, etc. without explicit permission. If you want to use it in merchandising please ask me first.',
		"Please do not just randomly repost the art with no mention that you didn't draw it. If you are on a website you aren't sure that im on, just say that it was made by Yugen the Corgi or corgiyugy on twitter/instagram, etc.! Just be cool about it. You can make it your pfp, use it how you want, just dont claim it as your own.",
		'f you buy an adopt from me, however it is 100% yours. Do not trace/steal the art or claim you made the art but besides that there are no limits. It is your character to own and keep.',
		'I am open to drawing pretty much anything, as long as I have a vague idea of what it is. If it is a species I am not 100% familiar with or i think I might have trouble with it, i will inform you so you can make the decision if you want to go through with it!',
		'If you are someone commissioning me and you are not a close friend I frequently talk to, please do NOT talk about innapropriate or upsetting topics to me. Topics include suicide, guns, NSFW, intense venting, drugs, politics, family issues. Etc.',
		'If these topics are brought up and I am made uncomfortable, I have full permission to refund you and cease the commission immediately. This will also prevent any future commissions possible from you.'
	]
};
let payment = {
	kind: 'Payment',
	points: [
		'All Payment is through Paypal or Ko-Fi right now!',
		'Payments must be paid in-full and upfront unless we aggree of a Payment plan that is suitable.',
		'I will not begin the commission until the Payment is made, but will gladly put you on my queue.',
		'I prefer not to make payment plans for anything below $30 but if it is more than that, im much more willing to negotiate with 50% up front and 50% at sketch',
		'If you are seeking a commission, please do not use my paypal.me! I will need the paypal email that is connected to your paypal, so I can send you an invoice! However, if you would like to use the paypal.me/yugycorgi for tips, feel free to as long as you communicate'
	]
};

let turnaround = {
	kind: 'Turnaround',
	points: [
		'I am someone who is currently enrolled in Highschool, soon to be college. I work a part time job, do this, spend time with my boyfriend and my family and try to have freetime to relax so if you buy a commission from me PLEASE do not bother me about when it will be done every day. If there are any updates about it, i WILL message you! feel free to ask every week or every few days, etc. as I want you to be comfortable but bothering me will just make me stressed and wont make it come faster!',
		'If you have an issue with how long your commission is taking, please let me know so I can figure out if there is a way i can move you ahead in the queue!',
		'If you need the commission done for something specific (a birthday, an event, etc.) and need a completion date, I am totally willing to discuss an expedited commission process that might cost a little bit more to move you to the top of the queue!',
		'Expect an update on when i begin the sketch, when i finish the sketch, and when it is complete! if you want to approve lineart (which occasionally I send to you anyways if I am unsure), please tell me that you would like to see lineart, coloring, etc. through the process and I will be happy to do that!'
	]
};
let refunds = {
	kind: 'Refunds',
	points: [
		'I am happy to give a full refund for a commission that I have not started, however if I have sent a sketch to you then you are no longer eligible for refund',
		'Refunds will be discussed on a situational basis',
		'If you make a commission with me, and completely go off the grid for 2 or more weeks or your account is completely suspended and you do not make an attempt to contact me on any other social media, etc. your commission will be cancelled with no refund.'
	]
};
module.exports = {
	name: 'yugen',
	updateToken: 'qwertzuiop1234567890',
	tos: [payment, turnaround, refunds, other],
	socials: {
		twitter: 'https://twitter.com/CorgiYugy',
		instagram: 'https://instagram.com/CorgiYugy',
		patreon: 'https://patreon.com/CorgiYugy',
		kofi: 'https://ko-fi.com/CorgiYugy',
		trello: 'https://trello.com/b/PTh1X2bw/yugen-the-corgi-commisions',
		discord: 'https://discord.gg/S9vbP3n'
	}
};