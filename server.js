//! Deps
const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');
const chalk = require('chalk');
const hbs = require('express-handlebars');
const UA = require('ua-parser-js');
const exec = require('shell-exec');
const simpleIcons = require('simple-icons');

const con = require('./constants');
let {
	port,
	hostname
} = {
	port: 3926
	//hostname: '67.182.206.28'
};

const app = express();

app.engine(
	'hbs',
	hbs({
		extname: 'hbs',
		defaultView: 'default',
		helpers: {
			ifeq: function (a, b, options) {
				if (a === b) {
					return true;
				}
			}
		}
	})
);

app.set('view engine', 'hbs');
app.set('json spaces', 4);
app.use('/assets', express.static('./assets'));
app.set('view options', {
	layout: false
});
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true
	})
);
app.use(helmet());
app.use(compression());
app.use(cors());
// Logging
app.use(
	morgan((tokens, req, res) => {
		return [
			chalk.hex('#34ace0').bold(`[ ${tokens.method(req, res)} ]`),
			chalk.hex('#ffb142').bold(tokens.status(req, res)),
			chalk.hex('#ff5252').bold(req.hostname + tokens.url(req, res)),
			chalk.hex('#2ed573').bold(tokens['response-time'](req, res) + 'ms'),
			chalk.hex('#f78fb3').bold('@ ' + tokens.date(req, res))
		].join(' ');
	})
);

module.exports = (client) => {
	app.get('/', async (req, res) => {
		/* 		var ua = UA(req.headers['user-agent']);
		console.log(ua) */
		res.render('index', {
			layout: 'main',
			name: con.name,
			host: req.hostname,
			item: [{
				name: 'TOS',
				source: '/tos'
			}],
			social: [{
					name: 'Twitter',
					link: 'twitter.com/CorgiYugy',
					icon: simpleIcons.Twitter
				},
				{
					name: 'Instagram',
					link: 'instagram.com/CorgiYugy',
					icon: simpleIcons.Instagram
				},
				{
					name: 'Patreon',
					link: 'Patreon.com/CorgiYugy',
					icon: simpleIcons.Patreon
				},
				{
					name: 'Ko-Fi',
					link: 'ko-fi.com/CorgiYugy',
					icon: simpleIcons['Ko-fi']
				},
				{
					name: 'Discord',
					link: 'discord.gg/S9vbP3n',
					icon: simpleIcons.Discord
				},
				{
					name: 'Trello',
					link: 'trello.com/b/PTh1X2bw/yugen-the-corgi-commisions',
					icon: simpleIcons.Trello
				}
			]
		});
	});

	app.get('/tos', async (req, res) => {
		/* 		var ua = UA(req.headers['user-agent']);
		console.log(ua) */
		res.render('tos', {
			layout: 'tos',
			name: con.name,
			host: req.hostname,
			WillDraw: ['Furries', 'Toony', 'Cell Shaded', 'Any Species', 'SFW', 'Candy-/Softgore'],
			WontDraw: ['NSFW', 'Hardgore', 'Feral (unless Chibi)', 'Realism', 'Humans', 'Hard fetish (ask)'],
			tos: con.tos
		});
	});

	app.get('/prices', async (req, res) => {
		res.sendFile(__dirname + '/assets/images/sheet.png')
	})

	app.get('/twitter', (req, res) => {
		res.redirect(con.socials.twitter);
	});
	app.get('/trello', (req, res) => {
		res.redirect(con.socials.trello);
	});
	app.get('/instagram', (req, res) => {
		res.redirect(con.socials.instagram);
	});
	app.get('/patreon', (req, res) => {
		res.redirect(con.socials.patreon);
	});
	app.get('/kofi', (req, res) => {
		res.redirect(con.socials.kofi);
	});
	app.get('/discord', (req, res) => {
		res.redirect(con.socials.discord);
	});

	app.listen(port /* , hostname */ , () => {
		console.log(`[ Server ] Listening on ${port}`);
	});
};